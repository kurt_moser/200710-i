<?php

function viewForm()
{
?>
<div class="container">
    <h3>Calculator</h3>
    <form action="" id="calc_form" method="POST">
        <div class="form-group">
            <label for="estimatedValue">Estimated value of the car</label>
            <input value="10000" id="calc_value" oninput="validateForm()" type="text" name="estimatedValue" class="form-control" id="estimatedValue" placeholder="100 - 100000">
        </div>
        <div class="form-group">
            <label for="taxPercentage">Tax percentage</label>
            <input value="10" id="calc_tax" oninput="validateForm()" type="text" name="taxPercentage" class="form-control" id="taxPercentage" placeholder="0 - 100">
        </div>
        <div class="form-group">
            <label for="numInstalments">Number of instalments</label>
            <select name="numInstalments" id="calc_instalments" class="form-control">
                <?php for ($i = 1; $i <= 12; $i++) { ?>
                    <option value="<?= $i ?>" <?= $i == 2 ? 'selected=' : '' ?>><?= $i ?></option>
                <?php } ?>
            </select>
        </div>
        <button onclick="fetchPolicyOffer()" type="button" id="calc_submit" class="btn btn-primary">Calculate</button>
    </form>
</div>
<script>
    function validateForm() {
        let formValue = document.getElementById('calc_value').value;
        let formTaxPercentage = document.getElementById('calc_tax').value;

        let isValid = true;

        if (isNaN(parseFloat(formValue)) || formValue < 100 || formValue > 100000) {
            isValid = false;
        }

        if (isNaN(parseFloat(formTaxPercentage)) || formTaxPercentage < 0 || formTaxPercentage > 100) {
            isValid = false;
        }

        if (isValid) {
            document.getElementById('calc_submit').disabled = false;
        } else {
            document.getElementById('calc_submit').disabled = true;
        }
    }

    function fetchPolicyOffer() {
        let req = new XMLHttpRequest();
        let value = document.getElementById('calc_value').value;
        let tax = document.getElementById('calc_tax').value;
        let instalments = document.getElementById('calc_instalments').value;

        req.onreadystatechange = function() {
            if (req.readyState === XMLHttpRequest.DONE) {
                if (req.status === 200) {
                    document.getElementById('offer').innerHTML = req.responseText;
                }
            }
        };

        req.open('GET', 'ajax.php?value=' + value + '&tax=' + tax + '&instalments=' + instalments, true);
        req.send();
    }

    (function() {
        validateForm();
    })();
</script>
<?php
}

function viewIndex()
{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Insurance Calculator</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

<?php viewForm() ?>

<div id="offer"></div>

</body>
</html>
<?php
}

viewIndex();
