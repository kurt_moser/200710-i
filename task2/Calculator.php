<?php

class Calculator
{
    const DEFAULT_COMMISSION_PERCENTAGE = 17;

    /**
     * Underlying object (car, etc) value
     *
     * @var float
     */
    protected $value;

    /**
     * Number of instalments policy is paid in
     *
     * @var int
     */
    protected $instalments;

    /**
     * Policy base percentage
     *
     * @var float
     */
    protected $basePercentage;

    /**
     * Policy commission percentage
     *
     * @var float
     */
    protected $commissionPercentage;

    /**
     * Policy tax percentage
     *
     * @var float
     */
    protected $taxPercentage;

    /**
     * Create calculator instance
     *
     * @param float $value
     * @param float $taxPercentage
     * @param integer $instalments
     */
    public function __construct(float $value, float $taxPercentage, int $instalments = 1)
    {
        $this->setValue($value)
            ->setInstalments($instalments)
            ->setBasePercentage($this->getDefaultBasePercentage())
            ->setCommissionPercentage(self::DEFAULT_COMMISSION_PERCENTAGE)
            ->setTaxPercentage($taxPercentage);
    }

    /**
     * Set policy base percentage
     *
     * @param float $basePercentage
     * @return Calculator
     */
    public function setBasePercentage(float $basePercentage): Calculator
    {
        $this->basePercentage = $basePercentage;

        return $this;
    }

    /**
     * Get policy base percentage
     *
     * @return float
     */
    public function getBasePercentage(): float
    {
        return $this->basePercentage;
    }

    /**
     * Set policy commission percentage
     *
     * @param float $commissionPercentage
     * @return Calculator
     */
    public function setCommissionPercentage(float $commissionPercentage): Calculator
    {
        $this->commissionPercentage = $commissionPercentage;

        return $this;
    }

    /**
     * Get policy commission percentage
     *
     * @return float
     */
    public function getCommissionPercentage(): float
    {
        return $this->commissionPercentage;
    }

    /**
     * Set policy tax percentage
     *
     * @param float $taxPercentage
     * @return Calculator
     */
    public function setTaxPercentage(float $taxPercentage): Calculator
    {
        $this->taxPercentage = $taxPercentage;

        return $this;
    }

    /**
     * Get policy tax percentage
     *
     * @return float
     */
    public function getTaxPercentage(): float
    {
        return $this->taxPercentage;
    }

    /**
     * Set underlying object (car, etc) value
     *
     * @param float $value
     * @return Calculator
     */
    public function setValue(float $value): Calculator
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get underlying object value
     *
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * Set number of instalments policy is paid in
     *
     * @param integer $instalments
     * @return Calculator
     * @throws \Exception if instalments is less than 1
     */
    public function setInstalments(int $instalments): Calculator
    {
        if ($instalments < 1) {
            throw new \Exception('Instalments must be positive');
        }

        $this->instalments = $instalments;

        return $this;
    }

    /**
     * Get number of instalments policy is paid in
     *
     * @return integer
     */
    public function getInstalments(): int
    {
        return $this->instalments;
    }

    /**
     * Get policy base price
     *
     * @return float
     */
    public function getBase(): float
    {
        return round($this->value * $this->basePercentage / 100.0, 2);
    }

    /**
     * Get policy commission
     *
     * @return float
     */
    public function getCommission(): float
    {
        return round($this->getBase() * $this->commissionPercentage / 100.0, 2);
    }

    /**
     * Get policy tax amount
     *
     * @return float
     */
    public function getTax(): float
    {
        return round($this->getBase() * $this->taxPercentage / 100.0, 2);
    }

    /**
     * Get policy total price
     *
     * @return float
     */
    public function getTotal(): float
    {
        // Rounding is likely unnecessary here as all components are already rounded
        return round($this->getBase() + $this->getCommission() + $this->getTax(), 2);
    }

    /**
     * Get single instalment base price
     *
     * @param boolean $firstInstalment
     * @return float
     */
    public function getInstalmentBase(bool $firstInstalment = false): float
    {
        if ($firstInstalment) {
            return $this->getBase() - ($this->instalments - 1) * $this->getInstalmentBase();
        }

        // PHP-s built-in round() method doesn't support rounding strictly down
        return (int)(($this->getBase() / $this->instalments) * 100) / 100.0;
    }

    /**
     * Get single instalment commission
     *
     * @param boolean $firstInstalment
     * @return float
     */
    public function getInstalmentCommission(bool $firstInstalment = false): float
    {
        if ($firstInstalment) {
            return $this->getCommission() - ($this->instalments - 1) * $this->getInstalmentCommission();
        }

        return (int)(($this->getCommission() / $this->instalments) * 100) / 100.0;
    }

    /**
     * Get single instalment tax amount
     *
     * @param boolean $firstInstalment
     * @return float
     */
    public function getInstalmentTax(bool $firstInstalment = false): float
    {
        if ($firstInstalment) {
            return $this->getTax() - ($this->instalments - 1) * $this->getInstalmentTax();
        }

        return (int)(($this->getTax() / $this->instalments) * 100) / 100.0;
    }

    /**
     * Get single instalment total price
     *
     * @param boolean $firstInstalment
     * @return float
     */
    public function getInstalmentTotal(bool $firstInstalment = false): float
    {
        if ($firstInstalment) {
            return $this->getTotal() - ($this->instalments - 1) * $this->getInstalmentTotal();
        }

        // Calculate through (base + commission + tax) to minimize rounding errors
        // Rounding is likely unnecessary here as all components are already rounded
        return round($this->getInstalmentBase() + $this->getInstalmentCommission() + $this->getInstalmentTax(), 2);
    }

    /**
     * Get policy base price percentage
     *
     * @return integer
     */
    public function getDefaultBasePercentage(): int
    {
        // On Fridays between 15:00 and 20:00 base percentage is bumped up
        if (date('w') == 5 && (int)date('Hi') >= 1500 && (int)date('Hi') <= 2000) {
            return 13;
        }

        return 11;
    }
}
