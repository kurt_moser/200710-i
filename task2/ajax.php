<?php

require('Calculator.php');

function viewPolicy()
{
    global $_GET;

    $instalments = (int)($_GET['instalments'] ?? 1);
    $value = (float)($_GET['value'] ?? 0);
    $taxPercentage = (float)($_GET['tax'] ?? 0);

    $calc = new Calculator($value, $taxPercentage, $instalments);

?>
<div class="container">
    <h3>Policy Offer</h3>
    <table class="table">
        <thead>
            <tr>
                <th></th>
                <th class="text-right">Policy</th>
                <?php for ($i = 0; $i < $instalments; $i++) { ?>
                    <th class="text-right"><?= $i + 1 ?>. Instalment</th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Value</td>
                <td class="text-right"><?= number_format($calc->getValue(), 2, '.', '') ?></td>
            </tr>
            <tr>
                <td>Base premium (<?= $calc->getBasePercentage() ?>%)</td>
                <td class="text-right"><?= number_format($calc->getBase(), 2, '.', '') ?></td>
                <td class="text-right"><?= number_format($calc->getInstalmentBase(true), 2, '.', '') ?></td>
                <?php for ($i = 0; $i < $calc->getInstalments() - 1; $i++) { ?>
                    <td class="text-right"><?= number_format($calc->getInstalmentBase(), 2, '.', '') ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td>Commission (<?= $calc->getCommissionPercentage() ?>%)</td>
                <td class="text-right"><?= number_format($calc->getCommission(), 2, '.', '') ?></td>
                <td class="text-right"><?= number_format($calc->getInstalmentCommission(true), 2, '.', '') ?></td>
                <?php for ($i = 0; $i < $calc->getInstalments() - 1; $i++) { ?>
                    <td class="text-right"><?= number_format($calc->getInstalmentCommission(), 2, '.', '') ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td>Tax (<?= $calc->getTaxPercentage() ?>%)</td>
                <td class="text-right"><?= number_format($calc->getTax(), 2, '.', '') ?></td>
                <td class="text-right"><?= number_format($calc->getInstalmentTax(true), 2, '.', '') ?></td>
                <?php for ($i = 0; $i < $calc->getInstalments() - 1; $i++) { ?>
                    <td class="text-right"><?= number_format($calc->getInstalmentTax(), 2, '.', '') ?></td>
                <?php } ?>
            </tr>
            <tr>
                <td class="font-weight-bold"><strong>Total</strong></td>
                <td class="text-right"><strong><?= number_format($calc->getTotal(), 2, '.', '') ?></strong></td>
                <td class="text-right"><strong><?= number_format($calc->getInstalmentTotal(true), 2, '.', '') ?></strong></td>
                <?php for ($i = 0; $i < $calc->getInstalments() - 1; $i++) { ?>
                    <td class="text-right"><strong><?= number_format($calc->getInstalmentTotal(), 2, '.', '') ?></strong></td>
                <?php } ?>
            </tr>
        </tbody>
    </table>
</div>
<?php
}

viewPolicy();
