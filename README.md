Used PHP version: 7.4.7\
Used MySQL server: 8.0.20

# Task 1

To run script:
```
task1> php index.php
```

Script contains also input decrypter (as commented out code).

# Task 2

To launch local server:
```
task2> php -S localhost:8000
```
After that calculator can be accessed from http://localhost:8000 in browser.

Calculator class consists of bunch of getters and setters. By default calculator is created using required percentages (11%/13% for base, 17% for commission, user entered % for tax), but it is configurable via setBasePercentage etc methods.

All calculations are done at run-time, i.e. no prices get precalculated into class properties for now.

To avoid problem where total price is not divisable by instalments count exactly a special case of first instalment price is supported. This first instalment price is calculated off the total price and n-1 instalment prices and it should always be >= other instalment prices.

All percentages and prices are floats, instalments is int, setting instalments <=0 throws exception.

Frontend uses vanilla Javascript and ajax to fetch policy offer based on form values.

# Task 3

- 001-up.sql - creates database schema
- 002-seed.sql - seeds tables with sample employee data
- 003-query.sql - contains SELECT query to fetch sample employee data
- 004-down.sql - removes database schema created by 001-up.sql

Employee data is split into 2 tables - 'employees' for main data and 'employee_descriptions' for text fields that can be in multiple languages (introduction/work experience/education). Contact info is stored as 3 separate fields: email/phone/address, also phone is stored as VARCHAR for now to support '+', '-', etc.

- employees.identity_code is set to be unique and is of VARCHAR type with the idea that some countries may use also letters, dashes, etc in identity code
- employees.created_at, created_by, updated_at, updated_by default to null for now
- employee_descriptions has unique key of (employee_id, lang) i.e. employee cannot have multiple description rows of the same language
- employee.created_by and modified_by are foreign keys pointing to table itself (employee.id). I've never used foreign key pointing to the same table and am not aware how good of an idea this is. Pay attention that for MySQL using such foreign key means that employee.id value must remain immutable - we cannot update id of employee who is creator/modifier of some other row as 'ON UPDATE CASCADE' will fail to update related rows of the same table (Postgres handles same situation without problems).
