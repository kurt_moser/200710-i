INSERT INTO employees
    (name, birthdate, identity_code, is_current_employee, email, phone, address)
VALUES
    ("Mary Ann O'Connez-Suslik", '2000-01-01', '60001019906', 1, 'maryann@oconnez.ee', '+37200000766', 'Tallinn, Mere pst 1-1');

SET @employee_id = LAST_INSERT_ID();

INSERT INTO employee_descriptions
    (employee_id, lang, introduction, work_experience, education)
VALUES
    (@employee_id, 'en', 'Introduction in english', 'Work experience in english', 'Education in english'),
    (@employee_id, 'es', 'Introduction in spanish', 'Work experience in spanish', 'Education in spanish'),
    (@employee_id, 'fr', 'Introduction in french', 'Work experience in french', 'Education in french');
