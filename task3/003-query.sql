SELECT
    employees.*,
    desc_en.introduction AS introduction_en,
    desc_en.work_experience AS work_experience_en,
    desc_en.education AS education_en,
    desc_es.introduction AS introduction_es,
    desc_es.work_experience AS work_experience_es,
    desc_es.education AS education_es,
    desc_fr.introduction AS introduction_fr,
    desc_fr.work_experience AS work_experience_fr,
    desc_fr.education AS education_fr
FROM
    employees
LEFT JOIN
    employee_descriptions AS desc_en ON employees.id=desc_en.employee_id AND desc_en.lang='en'
LEFT JOIN
    employee_descriptions AS desc_es ON employees.id=desc_es.employee_id AND desc_es.lang='es'
LEFT JOIN
    employee_descriptions AS desc_fr ON employees.id=desc_fr.employee_id AND desc_fr.lang='es'
WHERE
    employees.id=1
