CREATE TABLE employees (
    id INT UNSIGNED NOT NULL,
    name VARCHAR(255) COLLATE utf8mb4_unicode_ci NOT NULL,
    birthdate DATE,
    identity_code VARCHAR(50) COLLATE utf8mb4_unicode_ci NOT NULL,
    is_current_employee TINYINT(1),
    email VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
    phone VARCHAR(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
    address TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
    created_at TIMESTAMP NULL DEFAULT NULL,
    created_by INT UNSIGNED,
    updated_at TIMESTAMP NULL DEFAULT NULL,
    updated_by INT UNSIGNED
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE employees
    ADD PRIMARY KEY (id),
    ADD UNIQUE KEY(identity_code);

ALTER TABLE employees
    MODIFY id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    ADD CONSTRAINT created_by FOREIGN KEY (created_by) REFERENCES employees (id) ON DELETE SET NULL ON UPDATE CASCADE,
    ADD CONSTRAINT updated_by FOREIGN KEY (updated_by) REFERENCES employees (id) ON DELETE SET NULL ON UPDATE CASCADE;

CREATE TABLE employee_descriptions (
    id INT UNSIGNED NOT NULL,
    employee_id INT UNSIGNED NOT NULL,
    lang VARCHAR(2) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
    introduction TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
    work_experience TEXT COLLATE utf8mb4_unicode_ci NOT NULL,
    education TEXT COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE employee_descriptions
    ADD PRIMARY KEY (id),
    ADD UNIQUE KEY employee_id_lang (employee_id, lang);

ALTER TABLE employee_descriptions
    MODIFY id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    ADD CONSTRAINT employee_id FOREIGN KEY (employee_id) REFERENCES employees (id) ON DELETE CASCADE ON UPDATE CASCADE;
